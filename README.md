# ecowitt

A program to receive updates from the ecowitt GW1000 wireless gateway, and
format them to send along to Home Assistant via MQTT.


## The problem

I needed some cheap temperature sensors to track the temperature in my chicken
coop over the winter.  I wanted something that didn't rely on a hosted cloud
solution but that I could use stand alone at my own house.   The ecowitt weather
station was inexpensive and could handle up to 8 remote temperature sensors.
You could also configure it to send data to a server of your chosing.  This made
it a good enough choice to try out.  Ideally I'd have found something that was
open source, but I wasn't able to.  Once I had the temperature data, I wanted to
be able to take action based on the current temperature, for example to send
myself an alert if it went too low in the chicken coop.   As I was already using
Home Assistant for home automation, it made sense to try and get the data
there.

After researching sensors on Home Assistant, and how they work, it seemed like
the easiest and most flexible approach would be to create MQTT messages that I
could send to a listening MQTT server that Home Assistant would read from.  

## The solution

```plantuml
GW100 -> ecowitt: POST sensor data
ecowitt --> GW100: 200 OK
ecowitt -> "MQTT Broker": Configuration Messsage
ecowitt -> "MQTT Broker": State Updates
"MQTT Broker" -> "Home Assistant": Configuration Message
"MQTT Broker" -> "Home Assistant": State Updates
```

The GW1000 collects the data from connected sensors and sends it to a server
that you specify.  It sends it as a POST request over HTTP.  This program
(currently named ecowitt, but I'll probaby rename this as it seems to generic)
listens for a connection from the weather station, interprets it, and reformats
it into MQTT messages that Home Assistant can understand.

## Prior Art

I found a pre-existing solution to this problem:
https://github.com/iz0qwm/ecowitt_http_gateway 

But it did not quite fit my use-case since it doesn't directly put information
into Home Assistant.  

## Installation

### Prerequisites 

I assume that you alreday have:
- Home Assistant
- An MQTT broker
- Go
- This binary `go install gitlab.com/gabeguz/ecowitt`

### Setup

- Configure Home Assistant to consume MQTT messages from your MQTT broker.
- Configure this binary to send messages to the MQTT broker: `ecowitt -server
  tcp://<IP ADDRESS OF MQTT SERVER>:<PORT> -retained true`
- Configure your ecowitt GW1000 to send requests to this binary.


